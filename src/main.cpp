#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include "Ethernet_Generic.h"

#define THERMOCOUPLE 4
#define RREFR0 100 // = Rref/R0
#define BETA 3435  // EoS NTC



char timeServer[]         = "time.uni-freiburg.de";  // NTP server
unsigned int localPort    = 2390;             // local port to listen for UDP packets

const int NTP_PACKET_SIZE = 48;       // NTP timestamp is in the first 48 bytes of the message
const int UDP_TIMEOUT     = 2000;     // timeout in miliseconds to wait for an UDP packet to arrive

byte packetBuffer[NTP_PACKET_SIZE];   // buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;


EthernetServer server(7426);


SPISettings spisettings(1000000, MSBFIRST, SPI_MODE1);
SPISettings spisettingsW5100S(1000000, MSBFIRST, SPI_MODE0);

byte mac[] = {0x00, 0xA7, 0x7B, 0x43, 0x55, 0x4a };
IPAddress ip(192, 168, 2, 8);
IPAddress gw(192, 168, 2, 1);
IPAddress nm(255, 255, 255, 0);
IPAddress dnss(8,8,8,8);

EthernetClient client;
bool gotMessage = false;


void max31856Setup();
void max31856SetColdJ(float temp);
float max31856Read(uint8_t channel);
void max31856Debug();
void sht21Read(float* out);
void hyt939Read(uint8_t addr, float* out);
void hscRead(uint8_t addr, float* out);
void W5100SRegDump();
void sendNTPpacket(char *ntpSrv);
uint64_t ntpGetUnix();


const char* host = "djxmmx.net";
const uint16_t port = 17;
unsigned long byteCount = 0;
unsigned long beginMicros, endMicros;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  Serial.begin(115200);
  analogReadResolution(12);
  pinMode(20,OUTPUT); // ETH_RSTn
  pinMode(17,OUTPUT); // ETH_CS
  digitalWrite(20, 1); 
  digitalWrite(17, 1); 
  delay(100);
  
  max31856Setup();

  // I2C
  Wire.setSDA(12);
  Wire.setSCL(13);
  Wire.begin();

  Wire1.setSDA(10);
  Wire1.setSCL(11);
  Wire1.begin();

  Wire.beginTransmission(0x40);
  Wire.write(0xfe);
  Wire.endTransmission();
  
  // SPI0 for wiznet W5100S
  SPI.setRX(16);
  SPI.setTX(19);
  SPI.setSCK(18);
  SPI.setCS(17);
  SPI.begin(1);

  Ethernet.init(17);
  delay(2000);
  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found. ");
      //      return;
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, dnss);
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }

  Serial.print("Connected! IP address: ");
  Serial.println(Ethernet.localIP());

  if ( (Ethernet.getChip() == w5500) || (Ethernet.getChip() == w6100) || (Ethernet.getAltChip() == w5100s) )
  {
    if (Ethernet.getChip() == w6100)
      Serial.print("W6100 => ");
    else if (Ethernet.getChip() == w5500)
      Serial.print("W6100 => ");
    else
      Serial.print("W5100S => ");
    
    Serial.print("Speed: ");
    Serial.print(Ethernet.speedReport());
    Serial.print(", Duplex: ");
    Serial.print(Ethernet.duplexReport());
    Serial.print(", Link status: ");
    Serial.println(Ethernet.linkReport());
  }
  Udp.begin(localPort);
  server.begin();
}

// the loop routine runs over and over again forever:
void loop() {
  uint64_t unixtime = ntpGetUnix();
  
  float temp =  analogReadTemp();

  //  W5100SRegDump();
  
  // NTCs
  uint16_t ntc0 = analogRead(A0);
  uint16_t ntc1 = analogRead(A1);
  uint16_t ntc2 = analogRead(A2);
  uint16_t ntc3 = analogRead(A3);
  for(int i = 1; i < 16; i++){
    ntc0 += analogRead(A0);
    ntc1 += analogRead(A1);
    ntc2 += analogRead(A2);
    ntc3 += analogRead(A3);
    delay(30);
  }
  float t0 = ntc0/(65536.-ntc0)*RREFR0; // R/R0
  float t1 = ntc1/(65536.-ntc1)*RREFR0; // R/R0
  float t2 = ntc2/(65536.-ntc2)*RREFR0; // R/R0
  float t3 = ntc3/(65536.-ntc3)*RREFR0; // R/R0
  t0 = BETA*298.15/(BETA+log(t0)*298.15)-273.15;
  t1 = BETA*298.15/(BETA+log(t1)*298.15)-273.15;
  t2 = BETA*298.15/(BETA+log(t2)*298.15)-273.15;
  t3 = BETA*298.15/(BETA+log(t3)*298.15)-273.15;

  // thermocouples
  float sht[] = {NAN, NAN};
  sht21Read(sht);
  if(isfinite(sht[0])){
    max31856SetColdJ(sht[0]);
  }
  float tc0 = max31856Read(0);
  tc0 = max31856Read(0);
  float tc1 = max31856Read(1);
  float tc2 = max31856Read(2);
  float tc3 = max31856Read(3);
  float tc4 = max31856Read(4);
  float tc5 = max31856Read(5);
  float tc6 = max31856Read(6);
  float tc7 = max31856Read(7);
  max31856Debug();
  
  float hyt[] = {NAN, NAN};
  hyt939Read(0x28, hyt);
  float hsc[] = {NAN, NAN};
  hscRead(0x48, hsc);

  // printall
  Serial.printf("%lu,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",unixtime,temp,t0,t1,t2,t3,tc0,tc1,tc2,tc3,tc4,tc5,tc6,tc7,sht[0],sht[1],hyt[0],hyt[1],hsc[0],hsc[1]*10);
  // Serial.printf("SHT21: %f°C, %f%\n",sht[0], sht[1]);
   //   Serial.printf("HYT939: %f°C, %f\%\n",hyt[0], hyt[1]);
  //  Serial.printf("HSC: %f°C, %fbar\n",hsc[0], hsc[1]*10);
  EthernetClient client = server.available();
  if (client){
    client.connected();
    client.available();
    client.printf("%lu,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",unixtime,temp,t0,t1,t2,t3,tc0,tc1,tc2,tc3,tc4,tc5,tc6,tc7,sht[0],sht[1],hyt[0],hyt[1],hsc[0],hsc[1]*10);
    client.stop();
  }
}


void max31856Setup(){
  //  SPI1 for MAX31856
  SPI1.setRX(24);
  SPI1.setTX(15);
  SPI1.setSCK(14);
  SPI1.setCS(25);
  SPI1.begin(true);

  SPI1.beginTransaction(spisettings);
  // Open detext + Set 50HZ reject
  SPI1.transfer16(0x8001);

  // allow open to cause fault
  SPI1.transfer16(0x82ff);
  // // Set 16 samples and type T
  //SPI1.transfer16(0x8137);

  // Set 16 samples and type K
  SPI1.transfer16(0x8117);
  SPI1.endTransaction();

  // Setup ADG1607 swtich
  pinMode(0,OUTPUT); // A2
  pinMode(1,OUTPUT); // A1
  pinMode(2,OUTPUT); // A0
  pinMode(3,OUTPUT); // TCEN
  pinMode(8,INPUT);  // Fault
}

float max31856Read(uint8_t channel){
  // set switch
  digitalWrite(2, channel&1); // A0
  digitalWrite(1, (channel>>1)&1); // A1
  digitalWrite(0, (channel>>2)&1); // A2
  digitalWrite(3, 1); // TCEN

  delay(100);
  
  SPI1.beginTransaction(spisettings);
  uint8_t val = SPI1.transfer16(0x0001)&0x3f;
  // single shot conversion
  SPI1.transfer16(0x8040 | val);
  // wait for 16 conversions
  delay(185+20*15);
  char buf[3];
  memset(buf, 0, sizeof(buf));
  for(int i = 0 ; i < sizeof(buf); i++){
    buf[i] = SPI1.transfer16((i+0x0C)<<8)%256;
  }
  SPI1.endTransaction();
  // if(!digitalRead(8)){
  //   return NAN;
  // }
  float temp = 0;
  // Serial.printf("%x %x %x\n",buf[0], buf[1], buf[2]);
  if(buf[0] & 0x80){ // negative temp
    temp = ((buf[2]&0xe0)-0x100) * 0.000244140625;
    temp += (buf[1]-0xff) * .0625;
    temp += ((buf[0]&0x7f)-0x7f)*16;
  }
  else{ // positive temp
    temp = (buf[2]&0xe0) * 0.000244140625;
    temp += buf[1] * .0625;
    temp += buf[0]<<4;
  }
  digitalWrite(3, 1); // TCEN
  return temp;
}

void max31856SetColdJ(float temp){
  int16_t cjt = temp*64;
  cjt = cjt << 2;
  
  SPI1.beginTransaction(spisettings);
  // Set manual CJ 50HZ reject
  Serial.printf("CJT: %x %x\n", ((0xffff & cjt) >> 8) & 0xff , cjt &0xff);
  SPI1.transfer16(0x8089);

  SPI1.transfer16(0x8A00 | (((0xffff & cjt) >> 8) & 0xff));
  SPI1.transfer16(0x8B00 | (cjt & 0xff));

  SPI1.endTransaction();
}

void max31856Debug(){
  char buf[17];
  memset(buf, 0, sizeof(buf));
  SPI1.beginTransaction(spisettings);
  for(int i = 0 ; i < sizeof(buf); i++){
    buf[i] = SPI1.transfer16(i<<8)%256;
  }
  SPI1.endTransaction();
  Serial.printf("%x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7], buf[8], buf[9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15]);
}

void sht21Read(float* out){
  uint8_t buf[3];
  int i = 0;
  Wire.clearTimeoutFlag();
  Wire.beginTransmission(0x40);
  Wire.write(0xf3);
  Wire.endTransmission();
  delay(85);
  Wire.requestFrom(0x40,2);
  while(Wire.available() && i < 3) {
    buf[i] = Wire.read();    // Receive a byte as character
    i++;
  }
  if (Wire.getTimeoutFlag() || i<1){
    return;
  }
  int temp = buf[0]*256+buf[1];
  Wire.beginTransmission(0x40);
  Wire.write(0xf5);
  Wire.endTransmission();
  delay(30);
  Wire.requestFrom(0x40,2);
  i=0;
  while(Wire.available() && i < 4) {
    buf[i] = Wire.read();    // Receive a byte as character
    i++;
  }
  uint16_t humid = (buf[0]<<8) | buf[1];
  
  temp &= 0xfffc;
  humid &= 0xfff0;
  out[0] = -46.85+175.72*temp/0x10000;
  out[1] = -6.0 + humid*0.0019073486328125;

}


void hyt939Read(uint8_t addr, float* out){
  uint8_t buf[4];
  int i = 0;
  Wire.clearTimeoutFlag();
  Wire.beginTransmission(addr);
  Wire.endTransmission();
  delay(200);
  Wire.requestFrom(addr,4);
  while(Wire.available() && i < 4) {
    buf[i] = Wire.read();    // Receive a byte as character
    i++;
  }
  if (Wire.getTimeoutFlag() || i<3){
    return;
  }
  //  Serial.printf("%x %x\n", buf[0], buf[1]);
  uint16_t hum = ((buf[0]&0x1f)<<8) | buf[1];
  uint16_t temp = (buf[2]<<6) | (buf[3]>>2);
  out[0] = ((float) temp)*0.0100714-40;
  out[1] = ((float) hum)*0.006103888;
}

void hscRead(uint8_t addr, float* out){
  uint8_t buf[4];
  int i = 0;
  Wire.clearTimeoutFlag();
  Wire.requestFrom(addr,4);
  while(Wire.available() && i < 4) {
    buf[i] = Wire.read();    // Receive a byte as character
    i++;
  }
  if (Wire.getTimeoutFlag() || i<3){
    return;
  }
  uint16_t raw_pressure = buf[0]*256+buf[1];
  uint16_t raw_temp = buf[2]*8+buf[3]/32;
  out[1] = (raw_pressure-1638.)*1.03421/(14745-1638);
  out[0] = (raw_temp*200./2047)-50;

}

void W5100SRegDump(){
  for(int i = 0; i< 0x89; i++){
    digitalWrite(17,LOW);
    SPI.beginTransaction(spisettingsW5100S);
    SPI.transfer(0x0f);
    SPI.transfer(0);
    SPI.transfer(i);
    uint8_t ret = SPI.transfer(0);
    digitalWrite(17,HIGH);
    Serial.printf("W5100S: reg: %x   %x %i\n",i, ret, ret);
  }

}

// send an NTP request to the time server at the given address
void sendNTPpacket(char *ntpSrv)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)

  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(ntpSrv, 123); //NTP requests are to port 123

  Udp.write(packetBuffer, NTP_PACKET_SIZE);

  Udp.endPacket();
}

uint64_t ntpGetUnix(){
 
  sendNTPpacket(timeServer); // send an NTP packet to a time server

  // wait for a reply for UDP_TIMEOUT miliseconds
  unsigned long startMs = millis();

  while (!Udp.available() && (millis() - startMs) < UDP_TIMEOUT) {}

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  Udp.read(packetBuffer, NTP_PACKET_SIZE);
  uint64_t secsSince1900 = (packetBuffer[40]<<24) | (packetBuffer[41]<<16) | (packetBuffer[42]<<8) | packetBuffer[43];
  if(secsSince1900 == 0){
    return 0;
  }
  uint64_t unixtime = secsSince1900 - 2208988800;
  if( secsSince1900 < 3920969318){
    unixtime += 0x100000000;  // TODO: Update after 8.2.2036 and before 2160 to next era
  }
  return unixtime;
}
